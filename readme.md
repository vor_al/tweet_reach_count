A simple Laravel application that calculates the reach of a specific tweet.
User should be able to enter the URL of a tweet. The application will lookup the people who retweeted the tweet using the Twitter API.
The app then sums up the amount of followers each users has that has retweeted the tweet. These results is stored in the database.
If someone tries to calculate the reach of a tweet that has already been calculated the results should be returned from the database.
After two hours the database should be updated with new results.

(!!!) Work only if count of retweeters <= 100.
