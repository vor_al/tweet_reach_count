<?php

namespace App\Providers;

use App\Services\TwitterApi\TwitterApi;
use Illuminate\Support\ServiceProvider;

class TwitterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('TwitterApi', function ($app) {
            return new TwitterApi();
        });
    }

    public function provides()
    {
        return [
            'TwitterApi',
        ];
    }
}
