<?php

namespace App\Services\TwitterApi;

use DB;
use Symfony\Component\Process\Exception\RuntimeException;


class TwitterApi
{
    /**
     * @var string
     */
    private $access_token;

    /**
     * @var string
     */
    private $access_token_secret;

    /**
     * @var string
     */
    private $consumer_key;

    /**
     * @var string
     */
    private $consumer_secret;

    /**
     * Create the API access object. Requires an array of settings::
     * oauth access token, oauth access token secret, consumer key, consumer secret
     * These are all available by creating your own application on dev.twitter.com
     * Requires the cURL library
     *
     * @throws \RuntimeException When cURL isn't loaded
     */
    public function __construct()
    {
        if (!function_exists('curl_init'))
        {
            throw new RuntimeException('TwitterApi requires cURL extension!');
        }

        $this->access_token = env('TWITTER_ACCESS_TOKEN');
        $this->access_token_secret = env('TWITTER_ACCESS_TOKEN_SECRET');
        $this->consumer_key = env('TWITTER_CONSUMER_KEY');
        $this->consumer_secret = env('TWITTER_CONSUMER_SECRET');
    }

    /**
     * build data from GET request
     * @param string $string
     * @return string
     */
    private function buildGetFields ($string)
    {
        $getFields = preg_replace('/^\?/', '', explode('&', $string));
        $params = array();

        foreach ($getFields as $field)
        {
            if ($field !== '')
            {
                list($key, $value) = explode('=', $field);
                $params[$key] = $value;
            }
        }

        return '?' . http_build_query($params);
    }

    /**
     * build data from POST request
     * @param array $array - POST data
     * @return mixed
     */
    private function buildPostFields ($array)
    {
        if (isset($array['status']) && substr($array['status'], 0, 1) === '@')
        {
            $array['status'] = sprintf("\0%s", $array['status']);
        }

        foreach ($array as $key => &$value)
        {
            if (is_bool($value))
            {
                $value = ($value === true) ? 'true' : 'false';
            }
        }
        return $array;
    }

    /**
     * Bearer Twitter authorization
     * @return mixed - bearer token
     */
    private function getBearerToken ()
    {
        $basic_credentials = base64_encode($this->consumer_key . ':' . $this->consumer_secret);

        $tk = curl_init('https://api.twitter.com/oauth2/token');
        curl_setopt($tk, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$basic_credentials, 'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'));
        curl_setopt($tk, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
        curl_setopt($tk, CURLOPT_RETURNTRANSFER, true);
        $token = json_decode(curl_exec($tk));
        curl_close($tk);

        return $token;
    }

    /**
     * common function for get data from Twitter
     * @param $data
     * @param $method - post | get
     * @param $url - main url
     * @return mixed
     * @throws \Exception
     */
    private function curlRequest($data, $method, $url)
    {
        $bearer_token = $this->getBearerToken();

        $options = array(
                CURLOPT_HTTPHEADER => array('Authorization: Bearer ' . $bearer_token->access_token),
                CURLOPT_HEADER => false,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 10,
            );

        if ($method == 'post')
        {
            $options[CURLOPT_POSTFIELDS] = http_build_query($data);
        }
        else
        {
            $options[CURLOPT_URL] .= $data;
        }

        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);

        $this->httpStatusCode = curl_getinfo($feed, CURLINFO_HTTP_CODE);

        if (($error = curl_error($feed)) !== '')
        {
            curl_close($feed);
            throw new \Exception($error);
        }
        curl_close($feed);

        return $json;
    }

    /**
     * get Twitter result from POST request
     * @param $fields
     * @param $url
     * @return mixed
     * @throws \Exception
     */
    public function getDataFromPost ($fields, $url)
    {
        $oauth = [];
        $postFields = $this->buildPostFields($fields);

        if (!is_null($postFields)) {
            foreach ($postFields as $key => $value) {
                $oauth[$key] = $value;
            }
        }
        return $this->curlRequest($fields, 'post', $url);
    }

    /**
     * get Twitter result from GET request
     * @param $fields
     * @param $url
     * @return mixed
     * @throws \Exception
     */
    public function getDataFromGet ($fields, $url)
    {
        $oauth = [];
        $fields = $this->buildGetfields($fields);

        if (!is_null($fields))
        {
            $getFields = str_replace('?', '', explode('&', $fields));

            foreach ($getFields as $getField)
            {
                $split = explode('=', $getField);

                if (isset($split[1]))
                {
                    $oauth[$split[0]] = urldecode($split[1]);
                }
            }
        }
        return $this->curlRequest($fields, 'get', $url);
    }

    /**
     * get users ids
     * @param $tweet
     * @return mixed|string
     */
    public function getRetweeters ($tweet)
    {
        $url = 'https://api.twitter.com/1.1/statuses/retweeters/ids.json';
        $fields = '?id=' . $tweet . '&count=100&stringify_ids=true';

        return $this->getDataFromGet($fields, $url);
    }

    /**
     * get all data from Tweet
     * @param $tweet - tweet ID
     * @return mixed
     */
    public function getTweet ($tweet) {
        $url = 'https://api.twitter.com/1.1/statuses/show.json';
        $fields = '?id=' . $tweet;

        return $this->getDataFromGet($fields, $url);
    }

    /**
     * get count of retweeters from tweet
     * @param $tweet - tweet ID
     * @return int
     */
    public function getRetweetersCount ($tweet)
    {
        $tweet = $this->getTweet($tweet);
        $data = json_decode($tweet);
        return isset($data->retweet_count) ? $data->retweet_count : 0;
    }

    /**
     * get all data from tweeter user
     * @param $user_id - tweeter user ID
     * @return mixed
     */
    public function getUser ($user_id)
    {
        $url = 'https://api.twitter.com/1.1/users/show.json';
        $fields = '?user_id=' . $user_id;

        return $this->getDataFromGet($fields, $url);
    }

    /**
     * get count of Followers from user
     * @param $user_id - tweeter user ID
     * @return int
     */
    public function getFollowersCount ($user_id)
    {
        $user = $this->getUser($user_id);
        $data = json_decode($user);
        return isset($data->followers_count) ? $data->followers_count : 0;
    }

}