<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tweets';
}
