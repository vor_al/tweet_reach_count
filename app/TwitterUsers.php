<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwitterUsers extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'twitter_users';
}
