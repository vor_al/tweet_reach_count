<?php

namespace App\Http\Controllers;

use App\Services\TwitterApi\TwitterApi;
use App\Tweet;
use App\TwitterUsers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TweetController extends Controller
{
    public function __construct() {}

    /**
     * get Tweet id from url
     * @param string $url
     * @return string - Tweet id
     */
    private function _getTweetIdFromUrl ($url)
    {
        $path = parse_url($url);
        $path = trim($path['path'], '/');
        $path = substr($path, strrpos($path, '/'));
        return trim($path, '/');
    }

    /**
     * get data for view "result_count.blade.php"
     * @param Request $request - POST data
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addTweetData (Request $request)
    {
        // validate data from input
        $validator = Validator::make($request->all(), [
            'url' => 'required|max:64',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $ta = new TwitterApi();

        // search saved tweet data
        $tweet = Tweet::where('tweet_id', $this->_getTweetIdFromUrl($request->url))->first();
        $now = new Carbon();

        $message = '';
        // recount if more than 2 hours
        if (!($tweet && $now->lte($tweet->updated_at->addMinutes(env('TWITTER_MINUTES_4_SAVE'))))) {
            if (!$tweet) {
                $tweet = new Tweet();
                $tweet->tweet_id = $this->_getTweetIdFromUrl($request->url);
            }
            $tweet->retweeted_count = $ta->getRetweetersCount($tweet->tweet_id);
            $tweet_reach_count = 0;

            // search count followers if count of retweeted less than 100
            if ($tweet->retweeted_count <= env('TWITTER_MAX_RETWEETED')) {
                $users = json_decode($ta->getRetweeters($tweet->tweet_id), true);
                if ($users && $users['ids']) {
                    foreach ($users['ids'] as $uid) {
                        // search saved user
                        $user = TwitterUsers::where('twitter_user_id', $uid)->first();
                        // recount if more than 2 hours
                        if (!($user && $now->lte($user->updated_at->addMinutes(env('TWITTER_MINUTES_4_SAVE'))))) {
                            if (!$user) {
                                $user = new TwitterUsers();
                                $user->twitter_user_id = $uid;
                            }
                            $user->followers_count = $ta->getFollowersCount($uid);
                            $user->save();
                        }
                        $tweet_reach_count += $user->followers_count;
                    }
                }
                $tweet->reach_count = $tweet_reach_count;
            }
            $tweet->save();
        }

        if ($tweet->retweeted_count > env('TWITTER_MAX_RETWEETED')) {
            $message = 'Too much retweets';
        }

        return view('result_count', ['message' => $message, 'reach_count' => $tweet->reach_count, 'tweet_id' => $tweet->tweet_id]);
    }
}
