@extends('layouts.app')

@section('content')

    <div class="panel-body">

        @if ($message)
            <div class="alert alert-warning" style="text-align: center">
                <strong>Warning!</strong>
                <br/>
                {{ $message }}
            </div>
        @endif

        @if (!$message)
            <h2>Reach of tweet "{{ $tweet_id }}" - {{ $reach_count }}</h2>
        @endif

        <div class="col-sm-offset-3 col-sm-6">
            <button type="submit" class="btn btn-default" onclick="location.href='/'">
                <i class="fa"></i> New request
            </button>
        </div>
    </div>

@endsection