@extends('layouts.app')

@section('content')

    <div class="panel-body">
        @include('common.errors')

        <form action="/tweet" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="tweet-url" class="col-sm-3 control-label">Tweet URL</label>

                <div class="col-sm-6">
                    <input type="text" name="url" id="tweet-url" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Found reach count
                    </button>
                </div>
            </div>
        </form>
    </div>

@endsection